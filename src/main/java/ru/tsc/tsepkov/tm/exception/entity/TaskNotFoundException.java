package ru.tsc.tsepkov.tm.exception.entity;

import ru.tsc.tsepkov.tm.exception.field.AbstractFieldException;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
