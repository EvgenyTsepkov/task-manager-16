package ru.tsc.tsepkov.tm.service;

import ru.tsc.tsepkov.tm.api.ICommandRepository;
import ru.tsc.tsepkov.tm.api.ICommandService;
import ru.tsc.tsepkov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
