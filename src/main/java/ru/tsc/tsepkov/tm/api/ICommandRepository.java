package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
